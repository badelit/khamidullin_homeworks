package com.company;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList < Integer > list = new ArrayList < Integer > (); // Список для цифр
        ArrayList < Integer > minimum = new ArrayList < Integer > (); // Список для минимума цифр
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        while (a != -1) {

            while (a != 0) {
                list.add(a % 10); // Заполнение цифрами
                a = a / 10;
            }

            int min = list.get(0);
            for (int i = 0; i < list.size(); i++) {
                if (min > list.get(i)) {
                    min = list.get(i); // Поиск минимальной цифры
                }
            }

            minimum.add(min); // Добавление минимальной цифры входного числа
            a = scanner.nextInt();

        }

        int minDig = minimum.get(0);
        for (int i = 0; i < minimum.size(); i++) {
            if (minDig > minimum.get(i)) {
                minDig = minimum.get(i); // Поиск минимума среди всех минимумов
            }
        }
        System.out.println(minDig);
    }
}