package com.company;

import java.util.ArrayList;

public class Homework6 {

    //Реализовать функцию, принимающую на вход массив и целое число
    // Данная функция должна вернуть индекс этого числа в массиве.
    // Если число в массиве отсутствует - вернуть -1.
    public int getIndex(int[] ar, int k) {
        int temp = -1;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] == k) {
                temp = i;
            } else temp = -1;
        }
        return temp;
    }

    //Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые.
    public static void shiftZero(int[] a) {
        int t = 0;
        ArrayList < Integer > list = new ArrayList < Integer > ();
        for (int i = 0; i < a.length; i++) {
            if (a[i] != 0) {
                list.add(a[i]); // Добавляем в список все ненулевые
                t++;            // Считаем с какого начинаются нули
            }
        }

        for (int i = t; i < a.length; i++) {
            list.add(i, 0);  // Добавляем нули в конец списка с учетомм длины исходного массива
        }

        System.out.println(list + "" + t);
    }

    public static void main(String[] args) {
        int[] k = {4,5,2,0,7,90,0,0,0,23,5,9,5,7,1,0,0,323,0,0,43,76,0,356};

        shiftZero(k);

    }
}