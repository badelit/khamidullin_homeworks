import java.util.Comparator;

public class Human implements Comparator < Human > {
    private String name;
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Human(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public int compare(Human a, Human b) {
        if (a.weight < b.weight) return -1;
        else if (a.weight == b.weight) return 0;
        else return 1;
    }

}