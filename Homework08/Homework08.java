import java.util.Arrays;
import java.util.Comparator;

public class Homework08 {

    public static void main(String[] args) {

        Human[] humans = new Human[10];
        humans[0] = new Human("Bob", 60);
        humans[1] = new Human("John", 89);
        humans[2] = new Human("Zak", 59);
        humans[3] = new Human("Mike", 61);
        humans[4] = new Human("Dan", 190);
        humans[5] = new Human("Jack", 94);
        humans[6] = new Human("Bill", 86);
        humans[7] = new Human("Mathew", 79);
        humans[8] = new Human("Donald", 150);
        humans[9] = new Human("David", 92);

        for (int i = 0; i < humans.length; i++) {
            Arrays.sort(humans, new Human(humans[i].getName(), humans[i].getWeight()));
            System.out.println(humans[i].getName() + " " + humans[i].getWeight());

        }

    }

}