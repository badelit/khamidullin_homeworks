package homework9;

public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(8, 3, 6,4);
        Circle circle = new Circle(9, 9,5);
        Rectangle rectangle = new Rectangle(98, 64, 8, 3);
        Square square = new Square(7, 7, 9);


        System.out.println("Периметр эллипса равен: " + ellipse.getPerimeter());
        System.out.println("Периметр прямоугольника равен: " + rectangle.getPerimeter());
        System.out.println("Периметр квадрата равен: " + square.getPerimeter());
        System.out.println("Периметр круга равен: " + circle.getPerimeter());

    }
}