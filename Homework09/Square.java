package homework9;

public class Square extends Rectangle {


    public Square(double x, double y, double weight) {
        super(x, y, weight, weight);
    }

    public double getPerimeter() {
        return 4 * this.weight;
    }
}