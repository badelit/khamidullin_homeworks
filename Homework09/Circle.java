package homework9;

public class Circle extends Ellipse {


    public Circle(double x, double y, double r) {
        super(x, y, r, r);

    }

    public double getPerimeter() {
        return 2 * 3.14 * r;
    }
}