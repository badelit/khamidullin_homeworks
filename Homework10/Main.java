package Homework10;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(8, 3,8, 8);
        Rectangle rectangle = new Rectangle(98, 64,8,56);
        Square square = new Square(76, 7,9);
        Circle circle = new Circle(9, 29, 7 );


        Figure[] figure = new Figure[4];

        figure[0] = ellipse;
        figure[1] = rectangle;
        figure[2] = square;
        figure[3] = circle;



        for (int i = 0; i < figure.length; i++) {
            if (figure[i] instanceof Moveable) {
                ((Moveable) figure[i]).centerMove(0,0);
                 System.out.println(figure[i].getX() + " " + figure[i].getY());
            }


        }

    }
}