package Homework10;

public class Circle extends Ellipse implements Moveable {

    public Circle(double x, double y, double r) {
        super(x, y, r, r);

    }

    public double getPerimeter() {
        return 2 * 3.14 * r;
    }


    @Override
    public void centerMove(int x, int y) {
        this.x = x;
        this.y = y;
    }

}