package Homework10;

public class Rectangle extends Figure {

    protected double weight;
    private double length;

    public Rectangle(double x, double y, double weight, double length) {
        super(x, y);
        this.weight = weight;
        this.length = length;
    }

    public double getPerimeter() {
        return 2 * this.weight + 2 * this.length;

    }
}