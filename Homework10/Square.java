package Homework10;

public class Square extends Rectangle implements Moveable {

    public Square(double x, double y, double weight) {
        super(x, y, weight, weight);
    }

    public double getPerimeter() {
        return 4 * this.weight;
    }

    @Override
    public void centerMove(int x, int y) {
        this.x = x;
        this.y = y;
    }
}