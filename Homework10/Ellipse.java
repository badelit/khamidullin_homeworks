package Homework10;

public class Ellipse extends Figure {

    protected double r;
    private double r1;

    public Ellipse(double x, double y, double r, double r1) {
        super(x, y);
        this.r = r;
        this.r1 = r1;
    }


    public double getPerimeter() {
        return 4 * (3.14 * this.r * this.r1 + Math.pow((this.r - this.r1), 2)) / (this.r + this.r1);
    }

}
