package Homework10;

public abstract class Figure {
    protected double x;
    protected double y;
    protected double xc;
    protected double yc;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;

    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }




    public double getPerimeter() {
        return 0;
    }
}