import java.util.ArrayList;
import java.util.Scanner;

public class Homework07 {
    public static void main(String[] args) {

        ArrayList < Integer > list = new ArrayList < Integer > ();
        ArrayList < Integer > listCount = new ArrayList < Integer > ();
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int count = 0;
        while (a != -1) {

            list.add(a);
            a = scanner.nextInt(); // Запись с консоли в первый список.

        }
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {

                if (list.get(i) == list.get(j)) {
                    count++;

                }

            }
            listCount.add(i, count); // Добавление в список количества повторов для каждого элемента из первого списка.
            count = 0;
        }
        System.out.println(listCount); // Список повторов у каждого элемента.
        int min = listCount.get(0);
        int k = 0;
        for (int i = 0; i < listCount.size(); i++) {
            if (listCount.get(i) < min) {
                min = listCount.get(i); // Поиск минимума среди повторов.
                k = i;                  // Индекс минимально повторяющегося из первого списка.
            }

        }

        System.out.println(list.get(k)); // Если некоторые элементы имеют одинаковые повторы,
                                        // то выводится первый минималльный из первого списка.

    }

}